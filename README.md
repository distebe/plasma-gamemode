<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>
-->

![Screenie](https://invent.kde.org/sitter/plasma-gamemode/-/raw/master/picture.png)

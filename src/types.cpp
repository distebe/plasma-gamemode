// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include "types.h"

QDBusArgument& operator <<(QDBusArgument& argument, const GamesMapEntry& arg)
{
    argument.beginStructure();
    argument << arg.pid << arg.path;
    argument.endStructure();
    return argument;
}
const QDBusArgument& operator >>(const QDBusArgument& argument, GamesMapEntry& arg)
{
    argument.beginStructure();
    argument >> arg.pid >> arg.path;
    argument.endStructure();
    return argument;
}
